#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools
from setuptools import setup

with open('README.md') as readme_file:
    readme = readme_file.read()

setup(
    name='gitlab_metrics_scraper',
    version='0.0.1',
    description="",
    long_description=readme + '\n\n',
    author="Your Name",
    author_email='P.Harding@skatelescope.org',
    url='https://github.com/ska-telescope/sdi/gitlab-metrics',
    packages=setuptools.find_namespace_packages(where="src", include=["ska.*"]),
    entry_points = {
        "console_scripts": ['gitlab_metrics_scraper = ska.gitlab_metrics.gitlab_metrics:main']
        },
    package_dir={"": "src"},
    include_package_data=True,
    license="Apache2 license",
    zip_safe=False,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
    ],
    test_suite='tests',
    install_requires=[
        'aiohttp>=3.6.2',
        'aioprometheus>=20.0.0',
        'asyncio>=3.4.3',
        'gidgetlab>=0.6.0',
        'voluptuous>=0.11.7',
        'click',
        'pyyaml',
    ],
    setup_requires=[
        # dependency for `python setup.py test`
        'pytest-runner',
        # dependencies for `python setup.py build_sphinx`
        'sphinx',
        'recommonmark'
    ],
    tests_require=[
        'pytest',
        'pytest-cov',
        'pytest-json-report',
        'pycodestyle',
    ],
    extras_require={
        'dev':  ['prospector[with_pyroma]', 'yapf', 'isort'],
    }
)
