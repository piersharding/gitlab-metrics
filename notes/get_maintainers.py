#!/usr/bin/env python

import gitlab
import sys
import os

GITLAB_URL = 'https://gitlab.com/'
# project_name = 'piersharding/dask-operator'
# branch_name = 'master'

docker_compose_projects = []
with open('./projects.txt') as f:
    docker_compose_projects = f.read().splitlines()

print(docker_compose_projects)

def got_match(s):
    for l in docker_compose_projects:
        if l in s:
            return True
    return False

gl_token = os.getenv('GITLAB_TOKEN')

gl = gitlab.Gitlab(GITLAB_URL, private_token=gl_token, api_version=4)

try:
    gl.auth()
except GitlabAuthenticationError:
    log.error('Authentication error: did you provide a valid token?')
    sys.exit(1)
except GitlabGetError as err:
    response = json.loads(err.response_body.decode())
    log.error('Problem contacting Gitlab: {}'
        .format(response['message']))
    sys.exit(1)

print("ok")

skagroup = gl.groups.get('ska-telescope')
subgroups = skagroup.subgroups.list(all=True)

projs = 0
for group in ([skagroup] + subgroups):
    group = gl.groups.get(group.id)
    print("Group[{id}]: {name} description: {description}".format(id=group.id, name=group.name, description=group.description))
    try:
        for project in group.projects.list(all=True):
            project = gl.projects.get(project.id)
            if not got_match(project.name):
                continue
            projs += 1
            print("Project[{id}]: {name} description: {description} default_branch: {default_branch}".format(id=project.id, name=project.name_with_namespace, description=project.description, default_branch=project.default_branch))

            try:
                for member in project.members.list(all=True):
                    if member.access_level in [40, 50]:
                        print("Member[{id}]: {name} access_level: {level} (owner/maintainer)".format(id=member.id, name=member.name, level=member.access_level))
                print("\n")
            except:
                pass

    except AttributeError as e:
        pass

print("Total projects: {}".format(projs))
sys.exit(0)

print(project)
print(dir(project))

# print(dir(project.manager))
# print(project.manager.list())


for member in project.members.list():
    print("Member[{id}]: {name} access_level: {level} manager: {manager}".format(id=member.id, name=member.name, level=member.access_level, manager=member.manager))

    print(dir(member.manager))
    print(member.manager.all())
    # print("Manager[{id}]: {name}".format(id=member.manager.id, name=member.manager.name))
sys.exit(0)

branch = project.branches.get(branch_name)
top_commit = project.commits.get(branch.commit['short_id'])
pipelines = project.pipelines.list()



# print("\n\nProject:")
# print("\n\nProject[{}]:".format(project_name))
# print(project)
# print("\n\nBranch[{}]:".format(branch_name))
# print(branch)
# print("\n\nTop Commit:")
# print(top_commit)

first = pipelines.pop(0)
pipeline = project.pipelines.get(first.id)

# print("\n\nFirst Pipeline: {}".format(first))

# last pipeline status, coverage
print("\n\nThe Pipeline: {}".format(pipeline))

test_report_url = "{}api/v4/projects/{}/pipelines/{}/test_report".format(GITLAB_URL,project.id, pipeline.id)
print("\n\nThe URL: {}".format(test_report_url))
res = gl.http_get(test_report_url)

print(dir(res))
print(type(res))
print(res)

