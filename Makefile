# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

GITLAB_TOKEN ?= the-token
RTD_TOKEN ?= another-token
LOGGING_LEVEL ?= ERROR

.DEFAULT_GOAL := help

.PHONY: requirements run test lint clean build book help

# define private rules and additional makefiles
-include PrivateRules.mak

clean:  ## Clean build
	rm -rf build

requirements:  ## install the requirements for project
	# install node >=12 first https://computingforgeeks.com/how-to-install-nodejs-on-ubuntu-debian-linux-mint/
	poetry install
	poetry run jupyter labextension install jupyterlab-plotly

run:  ## Run the scraper
	PYTHONPATH=./src GITLAB_TOKEN=$(GITLAB_TOKEN) RTD_TOKEN=$(RTD_TOKEN) LOGGING_LEVEL=$(LOGGING_LEVEL) poetry run python gitlab_metrics_scraper.py

runtester:  ## Run tester
	PYTHONPATH=./src GITLAB_TOKEN=$(GITLAB_TOKEN) RTD_TOKEN=$(RTD_TOKEN) LOGGING_LEVEL=$(LOGGING_LEVEL) poetry run python get_proj.py

jupyter: ## Jupyter notebook for exploring commit data
	# poetry run pip install jupyter_contrib_nbextensions
	# poetry run jupyter contrib nbextension install --user
	poetry run jupyter-lab book/

book: ## Jupyter Book build
	cd book && rm -rf _build/ && poetry run jupyter-book build .

view: ## View current book
	sensible-browser book/_build/html/index.html

load: ## load commits from state files
	influx -host localhost -port 8086 -execute "drop measurement project_commits;"
	LOGGING_LEVEL=$(LOGGING_LEVEL) poetry run python tools/load-commit-state.py

build: clean  ## Build metrics scraper
	 poetry run python setup.py build

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
test:  ## Test metrics scraper
	 poetry run python setup.py test | tee ./build/setup_py_test.stdout; \
	 mv coverage.xml ./build/reports/code-coverage.xml;

fmt:  ## reformat code using black
	poetry run black src/ska
	poetry run isort --recursive src/ska

lint: pylint flake8  ## Lint check metrics scraper

pylint:  ## Lint check metrics scraper
	# FIXME pylint needs to run twice since there is no way go gather the text and junit xml output at the same time
	poetry run  pylint --output-format=parseable src/ska/gitlab_metrics | tee ./build/code_analysis.stdout; \
	poetry run pylint --output-format=pylint2junit.JunitReporter src/ska/gitlab_metrics > ./build/reports/linting.xml;

flake8: ## Lint with flake8
	poetry run flake8 src/ska

help:  ## show this help.
	@grep -hE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
