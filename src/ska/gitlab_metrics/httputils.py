# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.utils: main scraper class for collecting metrics using aioprometheus"""

import asyncio
import logging
import time

import aiohttp

log = logging.getLogger("gitlab_metrics")


class GitLabAIOHTTPMixin(object):
    """AIO HTTP Mixin class using aioinflux"""

    async def fetch(self, url):
        """Generic HTTP Client"""
        async with self.http_connector.get(url) as response:
            return response

    async def callAPI(self, url: str, once: bool = False):
        """Generic call of the GitLab API - tack on paging if required"""
        res = []
        try:
            thispage = 0
            url = "{base_url}{url}".format(base_url=self.config["gitlab_url"], url=url)
            while True:
                # check for throttle
                now = int(time.time())
                if not now == self.call_counter_now:
                    self.call_counter = 0
                self.call_counter_now = now
                self.call_counter += 1

                # if we have >= 10 this second, then sleep for a second
                if self.call_counter >= 10:
                    log.debug("Sleeping for a second to avoid throttle")
                    await asyncio.sleep(1)

                # Keep going until results less than a page
                thispage += 1
                if not once:
                    this_url = "{url}&per_page={per_page:d}&page={page:d}".format(
                        url=url, per_page=self.config["per_page"], page=thispage
                    )
                else:
                    this_url = url
                log.debug("[callAPI: {}] going for: {}".format(once, this_url))
                data = await self.gl_connector.getitem(this_url)
                log.debug("[callAPI] received: {}".format(len(data)))
                self.call_counter += 1
                # if only once, then bail
                if once:
                    return data
                # save results and check to go round again
                res.extend(data)
                if len(data) < self.config["per_page"]:
                    break
        except aiohttp.client_exceptions.ClientConnectorError as err:
            log.error("[callAPI] exception: {}".format(str(err)))
        return res
