# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.scraper: main scraper class for collecting metrics
using gidgetlab and aioinflux"""

import asyncio
import fnmatch
import logging
import os
import pickle
import re
import time
from asyncio.base_events import BaseEventLoop

import aiohttp
import gidgetlab.aiohttp
from aioinflux import InfluxDBClient

from .branches import GitLabBranchesMixin
from .httputils import GitLabAIOHTTPMixin
from .influxutils import GitLabAIOInfluxMixin
from .mergerequests import GitLabMergeRequestsMixin
from .projects import GitLabProjectsMixin
from .subgroups import GitLabSubGroupsMixin

log = logging.getLogger("gitlab_metrics")

METRICS_PREFIX = "gitlab_metrics_"


def match_regexp(glob_list: list, default: str):
    """Converts a list of glob matches into a single compiled regexp
    If list is empty, a compilation of default regexp is returned instead"""
    if not glob_list:
        return re.compile(default)
    return re.compile(r"|".join(fnmatch.translate(p) for p in glob_list))


class GitLabScraper(
    GitLabAIOHTTPMixin,
    GitLabAIOInfluxMixin,
    GitLabSubGroupsMixin,
    GitLabProjectsMixin,
    GitLabBranchesMixin,
    GitLabMergeRequestsMixin,
    object,
):
    """Main Scraper class using aioinflux"""

    def __init__(  # noqa: C901
        self,
        config: dict = {},
        loop: BaseEventLoop = None,
    ):

        self.config = config["global"]
        self.run_once = self.config["run_once"]
        self.db = self.config["db"]
        self.db_host = self.config["db_host"]
        self.db_port = self.config["db_port"]

        # set up state directory for pickles
        self.state_dir = self.config["state_dir"] + "/state"
        try:
            os.mkdir(self.state_dir)
        except FileExistsError as e:
            log.info(
                "state directory already exists: {}/{}".format(self.state_dir, str(e))
            )
        self.group_state_file = self.state_dir + "/groups.pickle"
        self.project_state_file = self.state_dir + "/projects.pickle"
        self.branch_state_file = self.state_dir + "/branches.pickle"
        self.badge_state_file = self.state_dir + "/badges.pickle"
        self.rtd_state_file = self.state_dir + "/rtds.pickle"
        self.mr_state_file = self.state_dir + "/mrs.pickle"

        # find group history pickle
        self.groups_loaded = False
        if os.path.isfile(self.group_state_file):
            with open(self.group_state_file, "rb") as f:
                self.group_state = pickle.load(f)
        else:
            self.group_state = {}

        # find project history pickle
        self.projects_loaded = False
        if os.path.isfile(self.project_state_file):
            with open(self.project_state_file, "rb") as f:
                self.project_state = pickle.load(f)
        else:
            self.project_state = {}

        # find branch history pickle
        self.branches_loaded = False
        if os.path.isfile(self.branch_state_file):
            with open(self.branch_state_file, "rb") as f:
                self.branch_state = pickle.load(f)
        else:
            self.branch_state = {}

        # find badge history pickle
        if os.path.isfile(self.badge_state_file):
            with open(self.badge_state_file, "rb") as f:
                self.badge_state = pickle.load(f)
        else:
            self.badge_state = {}

        # find rtd history pickle
        if os.path.isfile(self.rtd_state_file):
            with open(self.rtd_state_file, "rb") as f:
                self.rtd_state = pickle.load(f)
        else:
            self.rtd_state = {}

        # find mr history pickle
        if os.path.isfile(self.mr_state_file):
            try:
                with open(self.mr_state_file, "rb") as f:
                    self.mr_state = pickle.load(f)
            except Exception as e:
                log.debug(
                    "State file error: [{}] {}".format(self.mr_state_file, repr(e))
                )
                self.mr_state = {}
        else:
            self.mr_state = {}

        log.debug("Loaded state files")

        self.metrics_interval = self.config["metrics_interval"]
        self.sub_group_sleep = self.config["sub_group_sleep"]
        self.projects_sleep = self.config["projects_sleep"]
        self.branch_metrics_sleep = self.config["branch_metrics_sleep"]
        self.mr_metrics_sleep = self.config["mr_metrics_sleep"]
        self.stale_branches = self.config["stale_branches"] * 24 * 60 * 60
        self.stale_merge_requests = self.config["stale_merge_requests"] * 24 * 60 * 60
        self.loop = loop or asyncio.get_event_loop()
        self.exclude_metrics_re = match_regexp(
            self.config["exclude_metrics"], default=r"$."
        )
        log.debug("Exclude: " + repr(self.exclude_metrics_re))
        self.include_metrics_re = match_regexp(
            self.config["include_metrics"], default=r".*"
        )
        log.debug("Include: " + repr(self.include_metrics_re))
        self.exclude_labels = self.config["exclude_labels"]
        self.include_labels = self.config["include_labels"]
        self.projects = {}
        self.subgroups = {}
        self.project_branches = {}
        self.initialised_sub_groups = False
        self.initialised_projects = False
        self.initialised_branches = False
        # throttle tracking - GitLab API is 10 requests/sec
        self.call_counter = 0
        self.call_counter_now = int(time.time())

        jira_search = r".*[a-zA-Z][a-zA-Z0-9]+\-[0-9]+.*"
        self.jira_regex = re.compile(jira_search)

        # Setup the GitLab Connector
        self.gl_connector_httpclient = aiohttp.ClientSession()
        self.gl_connector = gidgetlab.aiohttp.GitLabAPI(
            self.gl_connector_httpclient,
            self.config["gitlab_user"],
            access_token=self.config["gitlab_token"],
        )

        # asyncio client for RTD calls
        headers = {
            "Authorization": "Token {token}".format(token=self.config["rtd_token"])
        }
        self.http_connector = aiohttp.ClientSession(headers=headers)

        # InfluxDB connector
        self.influxdb = InfluxDBClient(host=self.db_host, port=self.db_port, db=self.db)

        ######################################################################
        # Create application metrics and metrics service

    async def start_sub_groups(self):
        """Start the sub groups loop"""
        while True:
            log.info("start update for sub group")
            await self.process_sub_group()
            log.info("finished update for sub group")
            self.initialised_sub_groups = True
            if self.run_once:
                break
            log.info("process_sub_group sleeping for: {}".format(self.sub_group_sleep))
            await asyncio.sleep(self.sub_group_sleep)

    async def start_projects(self):
        """Start the projects loop"""
        while True:
            if self.initialised_sub_groups:
                log.info("start update for projects")
                await self.process_project()
                log.info("finished update for projects")
                self.initialised_projects = True
                if self.run_once:
                    break
            else:
                log.info("waiting for update for sub group")
            log.info(
                "process_project sleeping for: {}".format(
                    self.projects_sleep
                    if self.initialised_sub_groups
                    else self.metrics_interval
                )
            )
            await asyncio.sleep(
                self.projects_sleep
                if self.initialised_sub_groups
                else self.metrics_interval
            )

    async def start_branch_update_metrics(self):
        """Start the branch loop"""
        while True:
            if self.initialised_projects:
                log.info("start update for branch metrics")
                await self.branch_update_metrics()
                log.info("finished update for branch metrics")
                self.initialised_branches = True
                if self.run_once:
                    break
            else:
                log.info("branch_update_metrics waiting for update for projects")
            log.info(
                "branch_update_metrics sleeping for: {}".format(
                    self.branch_metrics_sleep
                    if self.initialised_projects
                    else self.metrics_interval
                )
            )
            await asyncio.sleep(
                self.branch_metrics_sleep
                if self.initialised_projects
                else self.metrics_interval
            )

    async def start_merge_request_update_metrics(self):
        """Start the merge requests loop"""
        while True:
            if self.initialised_projects:
                log.info("start update for merge request metrics")
                await self.merge_request_update_metrics()
                log.info("finished update for merge request metrics")
                if self.run_once:
                    break
            else:
                log.info("merge_request_update_metrics waiting for update for projects")
            log.info(
                "merge_request_update_metrics sleeping for: {}".format(
                    self.mr_metrics_sleep
                    if self.initialised_projects
                    else self.metrics_interval
                )
            )
            await asyncio.sleep(
                self.mr_metrics_sleep
                if self.initialised_projects
                else self.metrics_interval
            )
        # should only get here if run_once
        log.info("Terminiating from run_once")

    async def stop(self):
        """Stop all the Connections"""
        await self.influxdb.close()
        await self.gl_connector_httpclient.close()
        await self.http_connector.close()
        log.info("All done!")
