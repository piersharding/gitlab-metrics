# -*- coding: utf-8 -*-

"""Module init code."""


__version__ = "0.1.0"

__author__ = "Piers Harding"
__email__ = "Piers.Harding@skao.int"
