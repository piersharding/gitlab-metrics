# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.branches: branches metrics collection"""

import logging
import pickle
from datetime import datetime, timezone

import aiohttp
import gidgetlab
import gidgetlab.aiohttp

IGNORE_FOR_BRANCHES_TAG = "ignore_branches"

log = logging.getLogger("gitlab_metrics")


class GitLabBranchesMixin(object):
    """GitLab Branches Mixin class"""

    async def branch_update_metrics(self):  # noqa: C901
        """Update branch related metrics"""

        no_branches = 0
        # Branches
        points = 0
        measurement = "project_branches"
        projects = list(self.projects.values()).copy()
        for project in projects:
            # ignore projects that are probably external and have uncontrollable branches
            if "tag_list" in project and IGNORE_FOR_BRANCHES_TAG in project["tag_list"]:
                continue

            since = project["created_at"]
            try:
                log.debug("going for branches: " + repr(project))
                # sort: ensure last one run that has finished
                url = (
                    "/api/v4/projects/{project_id}"
                    "/repository/branches?simple=true&since={since}".format(
                        project_id=project["id"], since=since
                    )
                )
                branches = await self.callAPI(url)
            except (
                gidgetlab.exceptions.BadRequest,
                gidgetlab.exceptions.GitLabBroken,
                aiohttp.ClientOSError,
            ) as err:
                # get a bad request on snippets as it has no code in repo
                log.error(
                    "Error processing branch metrics: {0} for {1}/{2}".format(
                        repr(err), project["id"], project["path_with_namespace"]
                    )
                )
                continue

            if not project["id"] in self.project_branches:
                self.project_branches[project["id"]] = {}
            for branch in branches:
                # skip merged branches
                # if branch['merged']:
                #     continue
                no_branches += 1

                # add the branch to the list on known branches for this project
                if not branch["name"] in self.project_branches[project["id"]]:
                    self.project_branches[project["id"]][branch["name"]] = branch

                # we need the last commit for age check
                isstale = 0
                jira = 0
                mr = 0
                merged = 0
                default = 0
                if "commit" in branch and branch["commit"]:
                    # time since the last commit
                    try:
                        last_commit = datetime.strptime(
                            branch["commit"]["committed_date"], "%Y-%m-%dT%H:%M:%S.%f%z"
                        )
                    except ValueError:
                        from dateutil import parser

                        last_commit = parser.parse(branch["commit"]["committed_date"])

                    time_since = int(
                        (datetime.now(timezone.utc) - last_commit).total_seconds()
                    )
                    isstale = (
                        1
                        if time_since > self.stale_branches and not branch["merged"]
                        else 0
                    )
                    self.project_branches[project["id"]][branch["name"]][
                        "isstale"
                    ] = str(isstale)
                    merged = 1 if branch["merged"] else 0
                    self.project_branches[project["id"]][branch["name"]][
                        "merged"
                    ] = str(merged)
                    jira = 1 if self.jira_regex.match(str(branch["name"])) else 0
                    self.project_branches[project["id"]][branch["name"]]["jira"] = str(
                        jira
                    )
                    mr = (
                        1
                        if "mr" in self.project_branches[project["id"]][branch["name"]]
                        else 0
                    )
                    self.project_branches[project["id"]][branch["name"]]["mr"] = str(mr)
                    default = 1 if branch["default"] else 0
                    self.project_branches[project["id"]][branch["name"]][
                        "default"
                    ] = str(default)
                    self.project_branches[project["id"]][branch["name"]][
                        "time_since"
                    ] = time_since

                log.debug("[metric: {}] record: {}".format(measurement, branch))
                measure = {
                    "time": branch["commit"]["authored_date"],
                    "measurement": measurement,
                    "tags": {
                        "branch_id": branch["name"],
                        "project": project["path_with_namespace"],
                        "project_id": str(project["id"]),
                        "ref": branch["name"],
                        "merged": str(branch["merged"]),
                        "isstale": str(isstale),
                        "default": str(default),
                        "jira": str(jira),
                        "mr": str(mr),
                    },
                    "fields": {
                        "age": float(time_since),
                    },
                }
                delete_tags = {"branch_id": branch["name"], "project_id": project["id"]}
                log.debug("[metric: {}] record: {}".format(measurement, branch))
                await self.update_measurement(measurement, delete_tags, measure)
                points += 1

                # now update branch history bookmark
                if not project["id"] in self.branch_state:
                    self.branch_state[project["id"]] = {}
                self.branch_state[project["id"]][branch["name"]] = branch
                self.save_branch_state()

        log.info("[metric: {}] wrote: {}".format(measurement, points))
        self.branches_loaded = True

    def save_branch_state(self):
        """Pickle the branch state to cache"""
        # now update branch history bookmark
        with open(self.branch_state_file, "wb") as f:
            pickle.dump(self.branch_state, f)
            f.flush()
