# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.config: handler for configuration file"""

import logging

from voluptuous import ALLOW_EXTRA, All, Invalid, IsDir, Required, Schema, Url

log = logging.getLogger("gitlab_metrics")


class GitLabScraperConfig:
    def __init__(self):
        self.global_config_schema = Schema(
            {
                Required("db", default="gitlab_metrics"): str,
                Required("db_host", default="localhost"): str,
                Required("db_port", default=8086): int,
                Required("gitlab_url"): Url(),  # pylint: disable=no-value-for-parameter
                Required(
                    "state_dir", default="./"
                ): IsDir(),  # pylint: disable=no-value-for-parameter
                Required("gitlab_user"): str,
                Required("gitlab_token"): str,
                Required("rtd_token"): str,
                Required("per_page", default=100): int,
                Required("metrics_interval", default=60): int,
                Required("sub_group_sleep", default=3600): int,
                Required("projects_sleep", default=1800): int,
                Required("branch_metrics_sleep", default=1800): int,
                Required("mr_metrics_sleep", default=1800): int,
                Required("commit_metrics_sleep", default=1800): int,
                Required(
                    "stale_branches", default=180
                ): int,  # 180 days or 6 months or 2 PIs
                Required("commit_age", default=90): int,  # 90 days or 3 months or 1 PI
                Required(
                    "stale_merge_requests", default=30
                ): int,  # 30 days or 4 weeks or 2 Sprints
                Required("exclude_metrics", default=[]): list([str]),
                Required("include_metrics", default=[]): list([str]),
                Required("exclude_labels", default={}): Schema({}, extra=ALLOW_EXTRA),
                Required("include_labels", default={}): Schema({}, extra=ALLOW_EXTRA),
                Required("strip_labels", default=[]): list([str]),
                Required("include_subgroups", default=True): bool,
                Required("run_once", default=False): bool,
            }
        )
        self.config_schema = Schema(
            All(
                {
                    Required("global", default={}): self.global_config_schema,
                },
                self.check_url,
            )
        )

    @staticmethod
    def check_url(config):
        if "global" in config:
            if "gitlab_url" not in config["global"]:
                raise Invalid("gitlab_url must be set in the global.")
        return config
