# -*- coding: utf-8 -*-
"""ska.gitlab_metrics.__main__: executed when gitlab_metrics directory is called as script."""

# from .example import function_example
from .gitlab_metrics import main

main()
