# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.subgroups: sub group metrics collection"""

import logging
import pickle

log = logging.getLogger("gitlab_metrics")

# subgroup fields extracted
SUBGROUP_FIELDS = [
    "id",
    "name",
    "description",
    "path",
    "full_name",
    "full_path",
    "visibility",
    "created_at",
    "web_url",
]


class GitLabSubGroupsMixin(object):
    """GitLab Sub Group Mixin class"""

    async def process_sub_group(self):
        """Update application to simulate work"""
        url = "/api/v4/groups/{gitlab_user}/subgroups?simple=true".format(
            gitlab_user=self.config["gitlab_user"]
        )
        subgroups = await self.callAPI(url)

        # Handling sub groups
        points = 0
        measurement = "subgroups"

        for subgroup in subgroups:
            if subgroup["id"] in self.subgroups:
                for f in SUBGROUP_FIELDS:
                    self.subgroups[subgroup["id"]][f] = (
                        subgroup[f] if f in subgroup else ""
                    )
            else:
                self.subgroups[subgroup["id"]] = {
                    f: subgroup[f] if f in subgroup else "" for f in SUBGROUP_FIELDS
                }
            log.debug("[metric: {}] record: {}".format(measurement, subgroup))
            measure = {
                "time": subgroup["created_at"],
                "measurement": measurement,
                "tags": {
                    "id": subgroup["id"],
                    "subgroup": subgroup["full_path"],
                    "name": subgroup["name"],
                },
                "fields": {
                    "value": float(1),
                },
            }
            await self.update_measurement(measurement, {}, measure)
            points += 1

            # now update group history bookmark
            self.group_state[subgroup["id"]] = subgroup
            self.save_group_state()

        log.info("[metric: {}] wrote: {}".format(measurement, points))
        self.groups_loaded = True

    def save_group_state(self):
        """Pickle the group state to cache"""
        # now update group history bookmark
        with open(self.group_state_file, "wb") as f:
            pickle.dump(self.group_state, f)
            f.flush()
