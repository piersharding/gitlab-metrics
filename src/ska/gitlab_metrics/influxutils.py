# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.influxutils: AIO Influx Utils"""

import logging
import sys

import aiohttp
import aioinflux

log = logging.getLogger("gitlab_metrics")


class GitLabAIOInfluxMixin(object):
    """AIO Influx Mixin class using aioinflux"""

    async def db_connection_check(self):
        """Check connection to InfluxDB"""
        try:
            res = await self.influxdb.ping()
            log.debug("finished DB Check: {}".format(res))
        except aiohttp.client_exceptions.ClientConnectorError as e:
            log.error("DB Check failed: {}".format(e))
            sys.exit(1)

    async def update_measurement(self, measurement, tags, measure):
        """Update a measurement for a given series and tag set"""

        try:
            # only do the delete if it is not run_once - run_once is used for initialisation
            # if not self.run_once and tags:
            if tags:
                tags = " AND ".join([k + "='{}'".format(tags[k]) for k in tags.keys()])
                query = "DROP SERIES FROM {} WHERE {}".format(measurement, tags)
                log.debug(
                    "Query into InfluxDB: " + repr(query) + " tags: " + repr(tags)
                )
                await self.influxdb.query(q=query, db=self.db)
            measure["tags"] = {k: "{}".format(v) for (k, v) in measure["tags"].items()}
            log.debug("[metric: {}] record: {}".format(measurement, measure))
            await self.influxdb.write(measure)

        except (
            aioinflux.client.InfluxDBError,
            aioinflux.client.InfluxDBWriteError,
        ) as err:
            log.error("Error processing metrics into InfluxDB: " + repr(err))
