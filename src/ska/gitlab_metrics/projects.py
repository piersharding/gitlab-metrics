# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.projects: projects metrics collection"""

import logging
import pickle
from datetime import datetime, timezone

import aiohttp
import gidgetlab
import gidgetlab.aiohttp

log = logging.getLogger("gitlab_metrics")

# The Project fields extracted
PROJECT_FIELDS = [
    "id",
    "name",
    "description",
    "path",
    "path_with_namespace",
    "namespace",
    "web_url",
    "http_url_to_repo",
    "default_branch",
    "created_at",
    "last_activity_at",
]


class GitLabProjectsMixin(object):
    """GitLab Projects Mixin class"""

    async def process_project_group(self, group: str = None):  # noqa: C901
        """Update project for a group"""
        if group:
            url = "/api/v4/groups/{group}/projects?simple=true&license=true".format(
                group=group
            )
        else:
            url = (
                "/api/v4/groups/{gitlab_user}/projects?simple=true&license=true".format(
                    gitlab_user=self.config["gitlab_user"]
                )
            )
        data = await self.callAPI(url)
        log.info("Processing group: {} - {}".format(group, len(data)))
        projects = []
        for p in data:
            url = "/api/v4/projects/{project:d}?license=true".format(project=p["id"])
            projects.append(await self.callAPI(url, True))

        # stash each project
        measurement = "projects"
        processed = 0
        for project in projects:
            if (
                not self.include_metrics_re.match(project["path_with_namespace"])
                or self.exclude_metrics_re.match(  # noqa: W503
                    project["path_with_namespace"]
                )  # noqa: W503
                or project["archived"] is True  # noqa: W503
            ):
                log.info(
                    "NOT processing project: {} ({})".format(
                        project["path_with_namespace"],
                        ("archived" if project["archived"] is True else "regex"),
                    )
                )
                continue
            else:
                # update if already exists
                if "default_branch" not in project:
                    project["default_branch"] = "master"

                if project["id"] in self.projects:
                    for f in PROJECT_FIELDS:
                        self.projects[project["id"]][f] = (
                            project[f] if f in project else ""
                        )
                else:
                    self.projects[project["id"]] = {
                        f: project[f] if f in project else "" for f in PROJECT_FIELDS
                    }
                    for metric in [
                        "got_tests",
                        "got_linting",
                        "got_coverage",
                        "got_rtd",
                    ]:
                        self.projects[project["id"]][metric] = 0
                self.projects[project["id"]]["license"] = (
                    project["license"]["key"] if project["license"] else "unknown"
                )
                project["license"] = self.projects[project["id"]]["license"]

                # get the metrics
                try:
                    # Get the last run pipeline instance
                    # sort: ensure last one run that has finished
                    url = (
                        "/api/v4/projects/{project_id}"
                        "/pipelines?simple=true&per_page={per_page:d}"
                        "&page={page:d}&order_by=id&sort=desc"
                        "&ref={branch}".format(
                            project_id=project["id"],
                            per_page=1,
                            page=1,
                            branch=project["default_branch"],
                        )
                    )
                    data = await self.callAPI(url, True)
                    if len(data):
                        # if we have a pipeline then get the details
                        pipeline_id = data[0]["id"]
                        url = (
                            "/api/v4/projects/{project_id:d}"
                            "/pipelines/{pipeline_id:d}".format(
                                project_id=project["id"], pipeline_id=pipeline_id
                            )
                        )
                        data = await self.callAPI(url, True)

                        # time since the last pipeline execution
                        if not data["finished_at"]:
                            log.info("finished_at: {}".format(data["finished_at"]))
                            data["finished_at"] = datetime.fromtimestamp(
                                0, tz=timezone.utc
                            )
                            pipeline_finished = data["finished_at"]
                        else:
                            try:
                                pipeline_finished = datetime.strptime(
                                    data["finished_at"], "%Y-%m-%dT%H:%M:%S.%f%z"
                                )
                            except ValueError:
                                from dateutil import parser

                                pipeline_finished = parser.parse(data["finished_at"])

                        time_since = int(
                            (
                                datetime.now(timezone.utc) - pipeline_finished
                            ).total_seconds()
                        )
                        self.projects[project["id"]][
                            "pipeline_last_run_id"
                        ] = pipeline_id
                        self.projects[project["id"]][
                            "pipeline_time_since_last_run_seconds"
                        ] = time_since
                        self.projects[project["id"]]["pipeline_last_run_status"] = data[
                            "status"
                        ]
                        self.projects[project["id"]][
                            "pipeline_last_run_finished"
                        ] = pipeline_finished.timestamp()

                        # set the code coverage if it exists
                        if data["coverage"]:
                            self.projects[project["id"]]["last_coverage"] = float(
                                data["coverage"]
                            )

                        if data["duration"]:
                            self.projects[project["id"]][
                                "last_pipeline_duration"
                            ] = float(data["duration"])

                except (
                    gidgetlab.exceptions.BadRequest,
                    gidgetlab.exceptions.GitLabBroken,
                    aiohttp.ClientOSError,
                ) as err:
                    # get a bad request on snippets as it has no code in repo
                    # pass
                    log.error("Error processing projects: " + repr(err))

                # project level badges
                self.projects[project["id"]]["got_tests"] = 0
                self.projects[project["id"]]["got_linting"] = 0
                self.projects[project["id"]]["got_coverage"] = 0
                # take from the cache if we have it
                if project["id"] in self.badge_state:
                    self.projects[project["id"]]["got_tests"] = self.badge_state[
                        project["id"]
                    ]["got_tests"]
                    self.projects[project["id"]]["got_linting"] = self.badge_state[
                        project["id"]
                    ]["got_linting"]
                    self.projects[project["id"]]["got_coverage"] = self.badge_state[
                        project["id"]
                    ]["got_coverage"]
                else:
                    # poll GitLab for the state
                    try:
                        # Get the bages from the stdout file generated during test run
                        url = "/api/v4/projects/{project_id}/badges".format(
                            project_id=project["id"]
                        )
                        log.debug("going for badges: " + repr(url))

                        badges = await self.callAPI(url, True)
                        log.debug("badges: " + repr(badges))

                        self.projects[project["id"]]["got_tests"] = (
                            1
                            if await self.find_badge_item("test_total", badges, project)
                            else 0
                        )
                        self.projects[project["id"]]["got_linting"] = (
                            1
                            if await self.find_badge_item("lint_total", badges, project)
                            else 0
                        )
                        self.projects[project["id"]]["got_coverage"] = (
                            1
                            if await self.find_badge_item("coverage", badges, project)
                            else 0
                        )
                        self.badge_state[project["id"]] = {}
                        self.badge_state[project["id"]]["got_tests"] = self.projects[
                            project["id"]
                        ]["got_tests"]
                        self.badge_state[project["id"]]["got_linting"] = self.projects[
                            project["id"]
                        ]["got_linting"]
                        self.badge_state[project["id"]]["got_coverage"] = self.projects[
                            project["id"]
                        ]["got_coverage"]
                        self.save_badge_state()
                    except (
                        gidgetlab.exceptions.BadRequest,
                        gidgetlab.exceptions.GitLabBroken,
                        aiohttp.ClientOSError,
                    ) as err:
                        # get a bad request on snippets as it has no code in repo
                        log.error(
                            "Error processing badges metrics: {0} for {1}/{2}".format(
                                repr(err), project["id"], project["path_with_namespace"]
                            )
                        )

                # query RTD for existence of docs project
                self.projects[project["id"]]["got_rtd"] = 0
                # take from the cache if we have it
                if project["id"] in self.rtd_state:
                    self.projects[project["id"]]["got_rtd"] = self.rtd_state[
                        project["id"]
                    ]
                else:
                    # poll RTD for the state
                    try:
                        # Get the bages from the stdout file generated during test run
                        url = "https://readthedocs.org/api/v3/projects/{project_slug}/".format(
                            project_slug="ska-telescope-"
                            + project["path"].replace(".", "")  # noqa: W503
                        )
                        log.debug("going for rtd: " + repr(url))
                        rtd = await self.fetch(url)
                        log.debug("rtd: {0} [{1}]".format(url, rtd.status))
                        self.projects[project["id"]]["got_rtd"] = 0
                        if rtd.status == 200:
                            self.projects[project["id"]]["got_rtd"] = 1
                            log.debug(
                                "updated rtd for project: "
                                + repr(self.projects[project["id"]])  # noqa: W503
                            )
                        self.rtd_state[project["id"]] = self.projects[project["id"]][
                            "got_rtd"
                        ]
                        self.save_rtd_state()

                    except aiohttp.ClientError as err:
                        # get a bad request on snippets as it has no code in repo
                        log.info(
                            "Error processing rtd lookup: {0} for {1}/{2}".format(
                                repr(err), project["id"], project["path_with_namespace"]
                            )
                        )

                # now output the metric
                log.debug("[metric: {}] record: {}".format(measurement, project))
                measure = {
                    "time": project["created_at"],
                    "measurement": measurement,
                    "tags": {
                        "id": project["id"],
                        "project": project["path_with_namespace"],
                        "license": project["license"],
                        "pipeline_last_run_status": (
                            self.projects[project["id"]]["pipeline_last_run_status"]
                            if "pipeline_last_run_status"
                            in self.projects[project["id"]]
                            else "unknown"
                        ),
                        "got_tests": str(
                            self.projects[project["id"]]["got_tests"]
                            if "got_tests" in self.projects[project["id"]]
                            else 0
                        ),
                        "got_linting": str(
                            self.projects[project["id"]]["got_linting"]
                            if "got_linting" in self.projects[project["id"]]
                            else 0
                        ),
                        "got_coverage": str(
                            self.projects[project["id"]]["got_coverage"]
                            if "got_coverage" in self.projects[project["id"]]
                            else 0
                        ),
                        "got_rtd": str(
                            self.projects[project["id"]]["got_rtd"]
                            if "got_rtd" in self.projects[project["id"]]
                            else 0
                        ),
                    },
                    "fields": {
                        "pipeline_time_since_last_run_seconds": float(
                            self.projects[project["id"]][
                                "pipeline_time_since_last_run_seconds"
                            ]
                            if "pipeline_time_since_last_run_seconds"
                            in self.projects[project["id"]]
                            else 0
                        ),
                        "pipeline_last_run_finished": float(
                            self.projects[project["id"]]["pipeline_last_run_finished"]
                            if "pipeline_last_run_finished"
                            in self.projects[project["id"]]
                            else 0
                        ),
                        "last_coverage": float(
                            self.projects[project["id"]]["last_coverage"]
                            if "last_coverage" in self.projects[project["id"]]
                            else 0
                        ),
                        "last_pipeline_duration": float(
                            self.projects[project["id"]]["last_pipeline_duration"]
                            if "last_pipeline_duration" in self.projects[project["id"]]
                            else 0
                        ),
                    },
                }
                delete_tags = {"id": project["id"]}
                await self.update_measurement(measurement, delete_tags, measure)
                processed += 1

                # now update project history bookmark
                self.project_state[project["id"]] = project
                self.save_project_state()

        return processed

    async def process_project(self):
        """Update projects"""
        total_projects = await self.process_project_group()  # ungrouped projects
        for group in self.subgroups.values():
            total_projects = total_projects + await self.process_project_group(
                group["id"]
            )

        log.info("[metric: projects] wrote: {}".format(total_projects))
        self.projects_loaded = True

    def save_project_state(self):
        """Pickle the project state to cache"""
        # now update project history bookmark
        with open(self.project_state_file, "wb") as f:
            pickle.dump(self.project_state, f)
            f.flush()

    async def find_badge_item(self, item_name, items, project):
        """find whether the badge image actually exists for a metric"""
        ppath = project["path_with_namespace"] if project["path_with_namespace"] else ""
        branch = project["default_branch"] if project["default_branch"] else ""
        for item in items:
            if item["name"] == item_name:
                # now check the URL actually serves
                # https://nexus.engageska-portugal.pt/repository/raw/gitlab-ci-metrics/ska-telescope/sdi/bifrost/master/badges/lint_total.svg
                url = (
                    item["image_url"]
                    .replace("%{project_path}", ppath)
                    .replace("%{default_branch}", branch)
                )
                try:
                    res = await self.fetch(url)
                    log.debug(
                        "fetch image {0}: {1} [{2}]".format(item_name, url, res.status)
                    )
                    if res.status == 200:
                        return True
                    else:
                        return False
                except aiohttp.client_exceptions.ServerDisconnectedError as e:
                    log.error(
                        "fetch image failed! {0}: {1} [{2}]".format(item_name, url, e)
                    )
                    return False
        return False

    def save_badge_state(self):
        """Pickle the badge state to cache"""
        # now update badge history bookmark
        with open(self.badge_state_file, "wb") as f:
            pickle.dump(self.badge_state, f)
            f.flush()

    def save_rtd_state(self):
        """Pickle the rtd state to cache"""
        # now update rtd history bookmark
        with open(self.rtd_state_file, "wb") as f:
            pickle.dump(self.rtd_state, f)
            f.flush()
