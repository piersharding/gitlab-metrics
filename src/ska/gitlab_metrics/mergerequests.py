# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.mergerequests: Merge Request metrics collection"""

import logging
import pickle
from datetime import datetime, timezone

import aiohttp
import gidgetlab
import gidgetlab.aiohttp

log = logging.getLogger("gitlab_metrics")


class GitLabMergeRequestsMixin(object):
    """GitLab Merge Requests Mixin class"""

    async def merge_request_update_metrics(self):  # noqa: C901
        """Update Merge Request related metrics"""

        log.info(
            "merge requests - checking: {0} projects".format(
                len(self.projects.values())
            )
        )
        have_mrs = 0
        no_mrs = 0
        # Merge Requests
        points = 0
        measurement = "merge_requests"
        projects = list(self.projects.values()).copy()
        for project in projects:
            since = project["created_at"]
            try:
                # Get the merge request for project
                log.debug("going for: " + repr(project))
                url = (
                    "/api/v4/projects/{project_id}"
                    "/merge_requests?scope=all&state=all&since={since}".format(
                        project_id=project["id"], since=since
                    )
                )
                merge_requests = await self.callAPI(url)
            except (
                gidgetlab.exceptions.BadRequest,
                gidgetlab.exceptions.GitLabBroken,
                aiohttp.ClientOSError,
            ) as err:
                # get a bad request on snippets as it has no code in repo
                log.error(
                    "Error processing merge request metrics: {0} for {1}/{2}".format(
                        repr(err), project["id"], project["path_with_namespace"]
                    )
                )
                continue

            # Merge Requests on projects
            if not project["id"] in self.mr_state:
                self.mr_state[project["id"]] = {}

            if len(merge_requests) > 0:
                have_mrs += 1
            for mr in merge_requests:
                no_mrs += 1
                # skip merged MRs
                # if mr['state'] == "merged":
                #     continue
                # connect Merge Request to project/branch
                if "mrs" not in self.projects[project["id"]]:
                    self.projects[project["id"]]["mrs"] = {}
                # may not exist yet
                if (
                    project["id"] in self.project_branches
                    and mr["source_branch"]  # noqa: W503
                    in self.project_branches[project["id"]]
                ):
                    self.project_branches[project["id"]][mr["source_branch"]][
                        "mr"
                    ] = mr["id"]

                try:
                    last_commit = datetime.strptime(
                        mr["created_at"], "%Y-%m-%dT%H:%M:%S.%f%z"
                    )
                except ValueError:
                    from dateutil import parser

                    last_commit = parser.parse(mr["created_at"])

                time_since = int(
                    (datetime.now(timezone.utc) - last_commit).total_seconds()
                )
                if mr["state"] == "merged" and mr["merged_by"]:
                    merged_by = mr["merged_by"]["username"]
                else:
                    merged_by = "unknown"

                self.projects[project["id"]]["mrs"][mr["id"]] = mr
                jira = (
                    1
                    if (
                        self.jira_regex.match(str(mr["title"]))
                        or self.jira_regex.match(str(mr["description"]))  # noqa: W503
                    )
                    else 0
                )
                self.projects[project["id"]]["mrs"][mr["id"]]["jira"] = str(jira)
                branch_jira = (
                    1 if (self.jira_regex.match(str(mr["source_branch"]))) else 0
                )
                self.projects[project["id"]]["mrs"][mr["id"]]["branch_jira"] = str(
                    branch_jira
                )
                stale = (
                    1
                    if time_since > self.stale_merge_requests
                    and not (  # noqa: W503
                        mr["state"] == "merged" or mr["state"] == "closed"
                    )
                    else 0
                )
                self.projects[project["id"]]["mrs"][mr["id"]]["stale"] = str(stale)
                self.projects[project["id"]]["mrs"][mr["id"]]["merged"] = str(
                    1 if mr["state"] == "merged" else 0
                )
                self.projects[project["id"]]["mrs"][mr["id"]]["merged_by"] = merged_by
                self.projects[project["id"]]["mrs"][mr["id"]]["time_since"] = time_since

                measure = {
                    "time": mr["updated_at"],
                    "measurement": measurement,
                    "tags": {
                        "project_id": project["id"],
                        "project": project["path_with_namespace"],
                        "mr_id": str(mr["id"]),
                        "ref": mr["source_branch"],
                        "stale": str(stale),
                        "jira": str(jira),
                        "branch_jira": str(branch_jira),
                        "state": mr["state"],
                        "authorname": mr["author"]["name"],
                        "author": mr["author"]["username"],
                        "merged_by": mr["merged_by"],
                    },
                    "fields": {
                        "time_since": float(mr["time_since"]),
                    },
                }
                delete_tags = {"project_id": project["id"], "mr_id": mr["id"]}
                log.debug("[metric: {}] record: {}".format(measurement, mr))
                await self.update_measurement(measurement, delete_tags, measure)
                points += 1

                # now update mr history bookmark
                self.mr_state[project["id"]][mr["id"]] = self.projects[project["id"]][
                    "mrs"
                ][mr["id"]]
                self.save_mr_state()

        log.info("[metric: {}] wrote: {}".format(measurement, points))

        log.info("merge_request_update_metrics completed - MRs {}".format(no_mrs))
        log.info(
            "merge requests - {0} projects had MRs {1} did not have any".format(
                have_mrs, (len(self.projects.values()) - have_mrs)
            )
        )

    def save_mr_state(self):
        """Pickle the mr state to cache"""
        # now update mr history bookmark
        with open(self.mr_state_file, "wb") as f:
            pickle.dump(self.mr_state, f)
            f.flush()
