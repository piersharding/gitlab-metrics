#!/bin/sh

influx -host 192.168.99.37 -port 8086 -execute "drop database gitlab_metrics;"
influx -host 192.168.99.37 -port 8086 -execute "create database gitlab_metrics;"
influx -host 192.168.99.37 -port 8086 -database gitlab_metrics -execute "CREATE USER test WITH PASSWORD 'test';"
